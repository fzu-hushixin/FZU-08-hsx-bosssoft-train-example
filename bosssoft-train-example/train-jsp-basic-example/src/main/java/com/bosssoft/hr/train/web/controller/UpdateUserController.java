package com.bosssoft.hr.train.web.controller;

import com.bosssoft.hr.train.web.pojo.User;
import com.bosssoft.hr.train.web.service.UserService;
import com.bosssoft.hr.train.web.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 11:22
 * @since
 **/
@WebServlet(name = "updateUserController", urlPatterns = "/updateuser")
public class UpdateUserController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String code = req.getParameter("code");
        String password = req.getParameter("password");
        //校验参数合法性如果没问题才调用
        User user=new User();
        if (id != null && name != null && password != null){
            try{
                user.setId(Integer.parseInt(id));
                user.setName(name);
                user.setCode(code);
                user.setPassword(password);
                UserService userService = new UserServiceImpl();
                userService.update(user);
                req.getRequestDispatcher("/opSuccess.jsp").forward(req, resp);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }else{
            req.getRequestDispatcher("/opFaild.jsp").forward(req, resp);
        }


    }
}
