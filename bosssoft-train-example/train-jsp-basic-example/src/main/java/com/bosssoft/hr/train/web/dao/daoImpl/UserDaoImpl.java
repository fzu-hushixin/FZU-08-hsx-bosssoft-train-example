package com.bosssoft.hr.train.web.dao.daoImpl;

import com.bosssoft.hr.train.web.dao.UserDao;
import com.bosssoft.hr.train.web.pojo.User;
import com.bosssoft.hr.train.web.util.DBUtil;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:42
 * @since
 **/
public class UserDaoImpl implements UserDao {
    @Override
    public int insert(User user) throws Exception {
        int result;
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO t_user VALUES ('").append(user.getId())
                .append("', '").append(user.getName()).append("', '").
                append(user.getCode()).append("', '").append(user.getPassword()).
                append("')");
        String sql = sb.toString();
        result = DBUtil.executeUpdate(sql);
        return result;
    }

    @Override
    public int deleteById(Integer id) throws Exception {
        int result;
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM t_user WHERE id = ").append(id);
        String sql = sb.toString();
        result = DBUtil.executeUpdate(sql);
        return result;
    }

    /*
    根据id更新整条记录
     */
    @Override
    public int update(User user) throws Exception {
        int result;
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE t_user set name = '").append(user.getName()).
                append("', code = '").append(user.getCode()).
                append("', password = '").append(user.getPassword()).
                append("' WHERE id = ").append(user.getId());
        String sql = sb.toString();
        result = DBUtil.executeUpdate(sql);
        return result;
    }

    @Override
    public List<User> queryByCondition() throws Exception {
        List<User> users = new ArrayList<>();
        ResultSet resultSet = DBUtil.executeQuery("SELECT * FROM t_user WHERE 1=1");
        while (resultSet.next()) {
            User user = new User();
            user.setId(resultSet.getInt("id"));
            user.setName(resultSet.getString("name"));
            user.setCode(resultSet.getString("code"));
            user.setPassword(resultSet.getString("password"));
            users.add(user);
        }
        return users;
    }

    @Override
    public User findUser(String name, String password) throws Exception {
        User user = new User();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM t_user WHERE name = '").append(name)
        .append("' AND password = '").append(password).append("'");
        String sql = sb.toString();
        ResultSet resultSet = DBUtil.executeQuery(sql);
        while (resultSet.next())
        {
            user.setId(resultSet.getInt("id"));
            user.setName(resultSet.getString("name"));
            user.setCode(resultSet.getString("code"));
            user.setPassword(resultSet.getString("password"));
            return user;
        }
        return null;
    }


}
