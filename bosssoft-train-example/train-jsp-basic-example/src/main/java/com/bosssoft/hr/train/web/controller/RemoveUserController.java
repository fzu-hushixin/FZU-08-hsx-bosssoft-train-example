package com.bosssoft.hr.train.web.controller;

import com.bosssoft.hr.train.web.service.UserService;
import com.bosssoft.hr.train.web.service.impl.UserServiceImpl;
import lombok.SneakyThrows;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 11:22
 * @since
 **/
@WebServlet(name = "removeUserController", urlPatterns = "/removeuser")
public class RemoveUserController extends HttpServlet {
    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        doPost(req, resp);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String id = req.getParameter("id");
        int id1 = Integer.parseInt(id);
        UserService userService = new UserServiceImpl();
        if(userService.remove(id1)){
            req.getRequestDispatcher("/opSuccess.jsp").forward(req, resp);
        }
        req.getRequestDispatcher("/opFaild.jsp").forward(req, resp);
    }

}
