package com.bosssoft.hr.train.web.service;

import com.bosssoft.hr.train.web.pojo.User;

import java.util.List;

/**
 * @description: 用户服务的接口
 * @author: Administrator
 * @create: 2020-05-30 10:17
 * @since
 **/
public interface UserService {
    boolean save(User user);
    boolean remove(int id) throws Exception;
    boolean update(User user) throws Exception;
    List<User> queryByCondition() throws Exception;

    /**
     *  给 LoginController 调用
     * @param
     * @return
     */
    User authentication(String name, String password) throws Exception;

}
