package com.bosssoft.hr.train.web.controller;

import com.bosssoft.hr.train.web.dao.UserDao;
import lombok.SneakyThrows;
import com.bosssoft.hr.train.web.pojo.User;
import com.bosssoft.hr.train.web.service.UserService;
import com.bosssoft.hr.train.web.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:11
 * @since
 **/

@WebServlet(name = "loginController", urlPatterns = "/login")
public class LoginController extends HttpServlet {
    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        doPost(req, resp);
        /*String name = req.getParameter("name");
        String password = req.getParameter("password");
        UserService userService = new UserServiceImpl();
        User user = userService.authentication(name, password);
        if(user == null) {
            req.getRequestDispatcher("/success.jsp");
        }*/
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws UnsupportedEncodingException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        String name = req.getParameter("name");
        String password = req.getParameter("password");
        UserService userService = new UserServiceImpl();
        if(userService.authentication(name, password) != null){
            req.getSession().setAttribute("user", userService.authentication(name, password));
            req.getRequestDispatcher("/success.jsp").forward(req, resp);
        } else {
            req.getRequestDispatcher("/index.jsp").forward(req, resp);
            return;
        }
    }

    /**
     *  public 后面改为 private
     * @param code
     * @param password
     * @return
     */
    private boolean authentication(String code,String password){
        return false;
    }
}
