package com.bosssoft.hr.train.web.service.impl;

import com.bosssoft.hr.train.web.dao.UserDao;
import com.bosssoft.hr.train.web.dao.daoImpl.UserDaoImpl;
import com.bosssoft.hr.train.web.exception.BusinessException;
import com.bosssoft.hr.train.web.pojo.User;
import com.bosssoft.hr.train.web.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:24
 * @since
 **/

public class UserServiceImpl implements UserService {
    @Override
    public boolean save(User user) {
        try {
            UserDao userDao = new UserDaoImpl();
            if(userDao.insert(user) > 0) {
                return true;
            }
            return false;
        }catch (Exception ex){
            throw new BusinessException("10001",ex.getMessage(),ex);
        }
    }

    @Override
    public boolean remove(int id) throws Exception {
        UserDao userDao = new UserDaoImpl();
        if(userDao.deleteById(id) > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean update(User user) throws Exception {
        UserDao userDao = new UserDaoImpl();
        if(userDao.update(user) > 0) {
            return true;
        }
        return false;
    }

    @Override
    public List<User> queryByCondition() throws Exception {
        List<User> users = new ArrayList<>();
        UserDao userDao = new UserDaoImpl();
        users = userDao.queryByCondition();
        return users;
    }

    @Override
    public User authentication(String name, String password) throws Exception {
        UserDao userDao = new UserDaoImpl();
        User user = userDao.findUser(name, password);
        return user;
    }
}
