<%@ page contentType="text/html;charset=UTF-8" isErrorPage="false" language="java" isELIgnored="false" %>
<html>
<head>
    <title>success</title>
</head>
<body>
登录成功
<hr />
<h2>欢迎来到管理员页面</h2>
<br />
<p><b>添加用户 </b></p>
<form method="post" action="${pageContext.request.contextPath}/adduser">
    <label>id号</label><br />
    <input name="id" type="text" /><br />
    <label>name</label><br />
    <input name="name" type="text" /><br />
    <label>code</label><br />
    <input name="code" type="text" /><br />
    <label>password</label><br />
    <input name="password" type="text" /><br />
    <input type="submit" value="insert" />
</form>
<br />
<p><b>删除用户 </b></p>
<form method="post" action="${pageContext.request.contextPath}/removeuser">
    请输入要删除的用户ID<br />
    <input name="id" type="text" /><br />
    <input type="submit" value="delete" />
</form>
<br />
<p><b>更新用户 </b></p>
<form method="post" action="${pageContext.request.contextPath}/updateuser">
    <label>id号</label><br />
    <input name="id" type="text" /><br />
    <label>name</label><br />
    <input name="name" type="text" /><br />
    <label>code</label><br />
    <input name="code" type="text" /><br />
    <label>password</label><br />
    <input name="password" type="text" /><br />
    <input type="submit" value="update" />
</form>
</body>
</html>