package com.bosssoft.hr.train.collection;

import com.bosssoft.hr.train.pojo.Resource;
import com.bosssoft.hr.train.pojo.Role;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class HashMapExampleImplTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /*
    判断map中是否含键为role和值为resource的键值对
     */
    @Test
    public void put() {
        HashMapExampleImpl map = new HashMapExampleImpl();
        Map<Role,Resource> maps = map.getMap();
        Role role = new Role(1, "hsx");
        Resource resource = new Resource(1, "hsx");
        map.put(role, resource);
        Assert.assertEquals(true, maps.containsKey(role));
        Assert.assertEquals(true, maps.containsValue(resource));
    }

    /*
    判断删除后map中不存在键值对，同时map为空
     */
    @Test
    public void remove() {
        HashMapExampleImpl map = new HashMapExampleImpl();
        Map<Role,Resource> maps = map.getMap();
        Role role = new Role(1,"hsx");
        Resource resource = new Resource(1,"hsx");
        map.put(role, resource);
        map.remove(role);
        Assert.assertEquals(false, maps.containsKey(role));
        Assert.assertEquals(false, maps.containsValue(resource));
        Assert.assertEquals(true, maps.isEmpty());
    }

    /*
    判断该方法针对存在的键返回true，不存在的键返回false
     */
    @Test
    public void containsKey() {
        HashMapExampleImpl map = new HashMapExampleImpl();
        Map<Role,Resource> maps = map.getMap();
        Role role1 = new Role(1, "hsx");
        Resource resource = new Resource(1, "hsx");
        map.put(role1, resource);
        Role role2 = new Role(2, "lwx");
        Assert.assertEquals(true, map.containsKey(role1));
        Assert.assertEquals(false, map.containsKey(role2));
    }

    @Test
    public void visitByEntryset() {
        HashMapExample hashMapExample = new HashMapExampleImpl();
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.visitByEntryset();

    }

    @Test
    public void visitByKeyset() {
        HashMapExample hashMapExample = new HashMapExampleImpl();
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.visitByKeyset();
    }

    @Test
    public void visitByValues() {
        HashMapExample hashMapExample = new HashMapExampleImpl();
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.visitByValues();
    }
}