package com.bosssoft.hr.train.collection;

import com.bosssoft.hr.train.pojo.User;
import org.junit.Test;

import java.lang.reflect.Array;

import static org.junit.Assert.*;

public class TreeSetExampleImplTest {

    @Test
    public void sort() {
        TreeSetExmaple treeSetExample = new TreeSetExampleImpl();
        User user1 = new User(1,"zhangsan");
        User user2 = new User(2,"lisi");
        ((TreeSetExampleImpl) treeSetExample).add(user1);
        ((TreeSetExampleImpl) treeSetExample).add(user2);
        assertTrue(((TreeSetExampleImpl) treeSetExample).remove(user1));
        assertTrue(((TreeSetExampleImpl) treeSetExample).remove(user2));
    }
}