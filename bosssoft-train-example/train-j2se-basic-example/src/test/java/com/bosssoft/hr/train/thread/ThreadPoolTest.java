package com.bosssoft.hr.train.thread;

import org.junit.Test;

import static org.junit.Assert.*;

public class ThreadPoolTest {
    @Test
    public void test() {
        ThreadPool executor = new ThreadPool(4);
        try {
            for(int i=0;i<10;i++) {
                Runnable r = () -> {
                    System.out.println("j2se-basic-example-log : {}" + Thread.currentThread().getName());
                };
                executor.submit(r);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
        executor.shutdown();
    }

}