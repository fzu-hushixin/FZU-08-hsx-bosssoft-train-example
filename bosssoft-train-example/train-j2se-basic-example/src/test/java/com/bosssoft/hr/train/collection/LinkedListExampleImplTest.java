package com.bosssoft.hr.train.collection;

import com.bosssoft.hr.train.pojo.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.*;

public class LinkedListExampleImplTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void append() {
        LinkedListExampleImpl users = new LinkedListExampleImpl();
        LinkedList<User> users1 = users.getUsers();
        User user = new User(1, "hsx");
        Assert.assertEquals(true, users.append(user));
        Assert.assertEquals(user, users1.getFirst());
        Assert.assertEquals(user, users1.getLast());
        Assert.assertEquals(1, users1.size());
    }

    @Test
    public void get() {
        LinkedListExampleImpl users = new LinkedListExampleImpl();
        LinkedList<User> users1 = users.getUsers();
        User user = new User(1, "hsx");
        users.append(user);
        Assert.assertEquals(user, users.get(0));
    }

    @Test
    public void insert() {
        LinkedListExampleImpl users = new LinkedListExampleImpl();
        LinkedList<User> users1 = users.getUsers();
        User user1 = new User(1, "hsx");
        User user2 = new User(2, "lwx");
        User user3 = new User(3, "lqs");
        users.append(user1);
        users.append(user2);
        users.insert(1,user3);
        Assert.assertEquals(user1, users1.get(0));  //利用LinkedList自身函数去测试编写函数
        Assert.assertEquals(user2, users1.get(2));
        Assert.assertEquals(user3, users1.get(1));
    }

    @Test
    public void remove() {
        LinkedListExampleImpl users = new LinkedListExampleImpl();
        LinkedList<User> users1 = users.getUsers();
        User user1 = new User(1, "hsx");
        User user2 = new User(2, "lwx");
        User user3 = new User(3, "lqs");
        users.append(user1);
        users.append(user2);
        users.append(user3);
        users.remove(0);
        Assert.assertEquals(false, users1.contains(user1));
        users.remove(0);
        Assert.assertEquals(false, users1.contains(user2));
        users.remove(0);
        Assert.assertEquals(false, users1.contains(user3));
        Assert.assertEquals(true, users1.isEmpty());
    }

    @Test
    public void listByIndex() {
        LinkedListExmaple linkedListExample = new LinkedListExampleImpl();
        for(int i=0;i<10;i++){
            linkedListExample.append(new User(1,"hsx"));
        }
        linkedListExample.listByIndex();
    }

    @Test
    public void listByIterator() {
        LinkedListExmaple linkedListExample = new LinkedListExampleImpl();
        for(int i=0;i<10;i++){
            linkedListExample.append(new User(1,"hsx"));
        }
        linkedListExample.listByIterator();
    }

    @Test
    public void toArray() {
        LinkedListExmaple linkedListExample = new LinkedListExampleImpl();
        for(int i=0;i<5;i++){
            linkedListExample.append(new User(1,"hsx"));
        }
        linkedListExample.toArray();
    }

    @Test
    public void sort() {
        LinkedListExmaple linkedListExample = new LinkedListExampleImpl();
        for(int i=0;i<10;i++){
            linkedListExample.append(new User(1,"hsx"));
        }
        linkedListExample.sort();
    }

    @Test
    public void sort2() {
        LinkedListExampleImpl linkedListExample = new LinkedListExampleImpl();
        for(int i=0;i<10;i++){
            linkedListExample.append(new User(1,"hsx"));
        }
        linkedListExample.sort2();
    }

    @Test
    public void addFirst() {
        LinkedListExampleImpl users = new LinkedListExampleImpl();
        LinkedList<User> users1 = users.getUsers();
        User user1 = new User(1, "hsx");
        User user2 = new User(2, "lwx");
        User user3 = new User(3, "lqs");
        users.append(user1);
        users.append(user2);
        users.addFirst(user3);
        Assert.assertEquals(user3, users1.getFirst());
    }

    @Test
    public void offer() {
        LinkedListExampleImpl users = new LinkedListExampleImpl();
        LinkedList<User> users1 = users.getUsers();
        User user1 = new User(1, "hsx");
        User user2 = new User(2, "lwx");
        User user3 = new User(3, "lqs");
        users.append(user1);
        users.append(user2);
        users.offer(user3);
        Assert.assertEquals(user3, users1.getLast());
    }

    @Test
    public void sychronizedVisit() {
    }

    @Test
    public void push() {
        LinkedListExampleImpl users = new LinkedListExampleImpl();
        LinkedList<User> users1 = users.getUsers();
        User user1 = new User(1, "hsx");
        User user2 = new User(2, "lwx");
        User user3 = new User(3, "lqs");
        users.push(user1);
        Assert.assertEquals(true, users1.contains(user1));
        users.push(user2);
        Assert.assertEquals(user2, users1.getFirst());
        users.push(user3);
        Assert.assertEquals(user3, users1.getFirst());
    }

    @Test
    public void pop() {
        LinkedListExampleImpl users = new LinkedListExampleImpl();
        LinkedList<User> users1 = users.getUsers();
        User user1 = new User(1, "hsx");
        User user2 = new User(2, "lwx");
        User user3 = new User(3, "lqs");
        users.append(user1);
        users.append(user2);
        users.append(user3);
        Assert.assertEquals(user1, users.pop());
        Assert.assertEquals(false, users1.contains(user1));
        Assert.assertEquals(user2, users.pop());
        Assert.assertEquals(false, users1.contains(user2));
        Assert.assertEquals(user3, users.pop());
        Assert.assertEquals(false, users1.contains(user3));
    }
}