package com.bosssoft.hr.train.xml;

import com.bosssoft.hr.train.pojo.Student;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SAXOperatironTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    @Test
    public void query() {
        Student student = new Student(6, 21, "saxlqs");
        Assert.assertEquals(student, new SAXOperatiron().query(student));
    }
}