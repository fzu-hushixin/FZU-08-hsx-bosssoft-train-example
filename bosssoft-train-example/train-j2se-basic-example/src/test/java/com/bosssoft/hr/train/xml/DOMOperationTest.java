package com.bosssoft.hr.train.xml;

import com.bosssoft.hr.train.pojo.Student;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.swing.plaf.synth.SynthTableUI;

import static org.junit.Assert.*;

public class DOMOperationTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void create() {
        assertTrue(new DOMOperation().create(new Student(1, 18, "hsx")));
        assertTrue(new DOMOperation().create(new Student(2, 20, "lwx")));
        assertTrue(new DOMOperation().create(new Student(3, 22, "lqs")));
    }

    @Test
    public void remove() {
        assertTrue(new DOMOperation().remove(new Student(2, 20, "lwx")));
    }

    @Test
    public void update() {
        assertTrue(new DOMOperation().update(new Student(1, 20, "hsx")));
    }

    @Test
    public void query() {
        Student student = new Student(3, 22 ,"lqs");
        Assert.assertEquals(student, new DOMOperation().query(student));
    }
}