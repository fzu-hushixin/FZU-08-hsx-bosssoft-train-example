package com.bosssoft.hr.train.collection;

import com.bosssoft.hr.train.pojo.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ArrayListExampleImplTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /*
    判断添加数据前后列表长度是否相减为1
     */
    @Test
    public void append() {
        ArrayListExampleImpl userlist = new ArrayListExampleImpl();
        List<User> users = userlist.getUsers();
        int x = users.size();
        User user = new User();
        user.setId(1);
        user.setName("hsx");
        userlist.append(user);
        int y = users.size();
        int count = y - x;
        Assert.assertEquals(1,count);
    }

    /*
    判断添加数据后取得的数据是否为原数据
     */
    @Test
    public void get() {
        ArrayListExampleImpl userlist = new ArrayListExampleImpl();
        List<User> users = userlist.getUsers();
        User user = new User();
        user.setId(1);
        user.setName("hsx");
        userlist.append(user);
        User user1 = userlist.get(0);
        Assert.assertEquals(user,user1);
    }

    /*
    通过判断取出指定索引的内容是否与插入内容一致进行测试
     */
    @Test
    public void insert() {
        ArrayListExampleImpl userlist = new ArrayListExampleImpl();
        List<User> users = userlist.getUsers();
        User user1 = new User();
        user1.setId(1);
        user1.setName("hsx");
        User user2 = new User();
        user2.setId(2);
        user2.setName("lwx");
        User user3 = new User();
        user3.setId(3);
        user3.setName("lqs");
        userlist.append(user1);
        userlist.append(user2);
        userlist.insert(1,user3);
        Assert.assertEquals(user3,userlist.get(1));
    }

    /*
    通过判断删除前后长度相减是否为1和删除数据是否为选中数据进行测试
     */
    @Test
    public void remove() {
        ArrayListExampleImpl userlist = new ArrayListExampleImpl();
        List<User> users = userlist.getUsers();
        User user1 = new User();
        user1.setId(1);
        user1.setName("hsx");
        User user2 = new User();
        user2.setId(2);
        user2.setName("lwx");
        User user3 = new User();
        user3.setId(3);
        user3.setName("lqs");
        userlist.append(user1);
        userlist.append(user2);
        userlist.append(user3);
        int x = users.size();
        userlist.remove(1);
        int y = users.size();
        Assert.assertEquals(1,x - y);
        Assert.assertEquals(user3,userlist.get(1));
    }

    /*

     */
    @Test
    public void listByIndex() {
        ArrayListExampleImpl arrayListExample = new ArrayListExampleImpl();
        for(int i=0;i<5;i++) {
            arrayListExample.append(new User(i, "hsx" + i));
        }
        arrayListExample.listByIndex();
    }

    @Test
    public void listByIterator() {
        ArrayListExampleImpl arrayListExample = new ArrayListExampleImpl();
        for(int i=0;i<5;i++) {
            arrayListExample.append(new User(i, "hsx" + i));
        }
        arrayListExample.listByIterator();
    }

    @Test
    public void toArray() {
        ArrayListExmaple  arrayListExmaple = new ArrayListExampleImpl();
        for (int i=0;i<5;i++){
            arrayListExmaple.append(new User(i, "hsx" + i));
        }
        assertEquals((long)5,arrayListExmaple.toArray().length);
    }

    @Test
    public void sort() {
        ArrayListExmaple arrayListExmaple = new ArrayListExampleImpl();
        for (int i=0;i<5;i++){
            arrayListExmaple.append(new User(i, "hsx" + i));
        }
        arrayListExmaple.sort();
    }

    @Test
    public void sort2() {
        ArrayListExampleImpl arrayListExmaple = new ArrayListExampleImpl();
        for (int i=0;i<5;i++){
            arrayListExmaple.append(new User(i, "hsx" + i));
        }
        arrayListExmaple.sort2();
    }
}