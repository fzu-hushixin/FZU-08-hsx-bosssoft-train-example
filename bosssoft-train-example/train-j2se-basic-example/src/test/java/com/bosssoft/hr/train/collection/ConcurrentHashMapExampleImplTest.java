package com.bosssoft.hr.train.collection;

import com.bosssoft.hr.train.pojo.Resource;
import com.bosssoft.hr.train.pojo.Role;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConcurrentHashMapExampleImplTest {

    @Test
    public void put() {
        HashMapExample hashMapExample = new ConcurrentHashMapExampleImpl();
        hashMapExample.put(new Role(),new Resource());
        assertTrue(hashMapExample.size()!=0);
    }

    @Test
    public void remove() {
        HashMapExample hashMapExample = new ConcurrentHashMapExampleImpl();
        Role role = new Role();
        hashMapExample.put(role,new Resource());
        assertTrue(hashMapExample.size()!=0);
        hashMapExample.remove(role);
        assertTrue(hashMapExample.size()==0);
    }

    @Test
    public void containsKey() {
        HashMapExample hashMapExample = new ConcurrentHashMapExampleImpl();
        Role r = new Role();
        hashMapExample.put(r,new Resource());
        assertTrue(hashMapExample.containsKey(r));
        hashMapExample.remove(r);
        assertFalse(hashMapExample.containsKey(r));

    }

    @Test
    public void visitByEntryset() {
        HashMapExample hashMapExample = new ConcurrentHashMapExampleImpl();
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.visitByEntryset();
    }

    @Test
    public void visitByEntryset_Lambda() {
        HashMapExample hashMapExample = new ConcurrentHashMapExampleImpl();
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.visitByEntryset();
    }

    @Test
    public void visitByKeyset() {
        HashMapExample hashMapExample = new ConcurrentHashMapExampleImpl();
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.visitByKeyset();
    }

    @Test
    public void visitByValues() {
        HashMapExample hashMapExample = new ConcurrentHashMapExampleImpl();
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.put(new Role(),new Resource());
        hashMapExample.visitByValues();
    }
}