package com.bosssoft.hr.train.pojo;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:07
 * @since
 **/
public class Student extends User {
    private int age;
    public Student(int id,int age,String name){
        super(id,name);
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
