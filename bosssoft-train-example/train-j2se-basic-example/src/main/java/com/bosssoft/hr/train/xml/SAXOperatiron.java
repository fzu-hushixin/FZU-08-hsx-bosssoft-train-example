package com.bosssoft.hr.train.xml;

import com.bosssoft.hr.train.pojo.Student;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.ElementHandler;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:10
 * @since
 **/
public class SAXOperatiron implements XMLOperation<Student> {
    private  static String PATH = "src/test/resources/student.xml";

    private static boolean parse(String path, ElementHandler handler,String documentPath){
        SAXReader reader  = new SAXReader();
        boolean result = false;
        try{
            reader.addHandler(documentPath,handler);
            Document doc = reader.read(path);
            result = store(doc,path);
        }catch (DocumentException e){
            e.printStackTrace();
        }
        return result;
    }

    private static boolean store(Document document,String path){
        try{
            XMLWriter writer = new XMLWriter(
                    new OutputStreamWriter(new FileOutputStream(PATH)));
            writer.write(document);
            writer.close();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean create(Student object) {
        /*
        SAX解析方式不支持修改XML文件
         */
        return false;
    }

    @Override
    public boolean remove(Student object) {
        /*
        SAX解析方式不支持修改XML文件
         */
        return false;
    }

    @Override
    public boolean update(Student object) {
        /*
        SAX解析方式不支持修改XML文件
         */
        return false;
    }

    @Override
    public Student query(Student object) {
        Query query = new Query(object);
        parse(PATH,query,"/students/studnet");
        return object;
    }
}

