package com.bosssoft.hr.train.annotation;

import com.bosssoft.hr.train.database.DBUtil;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:41
 * @since
 **/
public class BaseModel {
    private Field[] fields = this.getClass().getDeclaredFields();

    public int save(UserModel userModel) throws Exception {
        int result = 0;
        Class c = userModel.getClass();
        Table table = (Table) c.getAnnotation(Table.class);
        StringBuilder sb = new StringBuilder();
        sb.append("insert into");
        sb.append(" ").append(table.value()).append(" values (").append(userModel.getId()
        + ",'" + userModel.getAge() + "','" + userModel.getName() + "')");
        String sql = sb.toString();
        result = DBUtil.executeUpdate(sql);
        return result;
    }

    /*
    根据id修改姓名和年龄
     */
    public int update(UserModel userModel) throws Exception {
        int result;
        Class c = userModel.getClass();
        Table table = (Table) c.getAnnotation(Table.class);
        StringBuilder sb = new StringBuilder();
        sb.append("update ");
        sb.append(table.value()).append(" set name = '").append(userModel.getName())
                .append("' , age = '").append(userModel.getAge()).append("' where id = ").
                append(userModel.getId());
        String sql = sb.toString();
        System.out.println(sql);
        result = DBUtil.executeUpdate(sql);
        return result;
    }

    /*
    根据id删除字段
     */
    public int remove(UserModel userModel) throws Exception {
        int result;
        Class c = userModel.getClass();
        Table table = (Table) c.getAnnotation(Table.class);
        StringBuilder sb = new StringBuilder();
        sb.append("delete from ");
        sb.append(table.value()).append(" where id = ").append(userModel.getId());
        String sql = sb.toString();
        result = DBUtil.executeUpdate(sql);
        return result;
    }

    public List queryForList() throws Exception {
        UserModel userModel = new UserModel();
        Class className = userModel.getClass();
        Table tableName = (Table) className.getAnnotation(Table.class);
        StringBuffer sb = new StringBuffer();
        sb.append("select * ");
        sb.append(" from ").append(tableName.value());
        String sql = sb.toString();
        ResultSet resultSet = DBUtil.executeQuery(sql);
        List<UserModel> users = new ArrayList<>();
        while(resultSet.next()) {
            UserModel user = new UserModel();
            user.setId(resultSet.getInt("id"));
            user.setAge(resultSet.getInt("age"));
            user.setName(resultSet.getString("name"));
            users.add(user);
        }
        return users;
    }

}
