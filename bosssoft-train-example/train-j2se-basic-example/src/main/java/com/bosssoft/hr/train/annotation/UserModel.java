package com.bosssoft.hr.train.annotation;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:42
 * @since
 **/
@Table(value = "user")
public class UserModel extends BaseModel {
    @Id(value = "id")
    private int id;
    @Column(value="name")
    private String name;
    @Column(value="age")
    private int age;

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserModel(int id, String name, int age) {
        this.id = id;
        this.age = age;
        this.name = name;
    }

    public UserModel() {}
}
