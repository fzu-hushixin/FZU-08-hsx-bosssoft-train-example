package com.bosssoft.hr.train.xml;


import com.bosssoft.hr.train.pojo.Student;
import org.dom4j.Element;
import org.dom4j.ElementHandler;
import org.dom4j.ElementPath;

public class Query implements ElementHandler {
    Student student;

    public Query(Student student){
        this.student= student;
    }
    @Override
    public void onEnd(ElementPath elementPath) {
        Element student  = elementPath.getCurrent();
        if(student.attribute("id").getValue().equals(String.valueOf(this.student.getId()))){
            this.student.setAge( Integer.valueOf(student.element("age").getText()));
            this.student.setName(student.element("name").getText());
        }
    }

    @Override
    public void onStart(ElementPath elementPath) {
    }
}