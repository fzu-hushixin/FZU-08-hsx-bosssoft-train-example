package com.bosssoft.hr.train.database;

import java.sql.*;

/**
 * @description:  我是工具类并且我不喜欢被继承 final 保护了我免于继承，private 保护我被创建
 * @author: Administrator
 * @create: 2020-05-28 20:45
 * @since
 **/
public final class DBUtil {
    static Connection conn = null;
    static Statement stmt = null;
    static PreparedStatement pstmt=null;

    static String ip = "rm-2vcu7g62h32787120lo.mysql.cn-chengdu.rds.aliyuncs.com";
    static int port = 3306;
    static String database = "j2se";
    static String loginName = "fzu_hushixin";
    static String password = "@hushixin123";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private DBUtil(){}


    private static Connection createConnection() throws Exception {
        String url = String.format("jdbc:mysql://%s:%d/%s?useUnicode=true&characterEncoding=UTF-8&" +
                "useSSL=false", ip, port, database);
        return DriverManager.getConnection(url, loginName, password);
    }

    private static void close(Statement stmt, Connection conn) throws Exception {
        if (stmt != null) {
            stmt.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    /**
     *  如果需要参数的你可以选择overload 改方法
     * @param sql
     * @return
     */
    public static ResultSet executeQuery(String sql) throws Exception {
        try {
            conn = createConnection();
            stmt = conn.createStatement();
            return stmt.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int executeUpdate(String sql) throws Exception {
        int result = 0;
        try {
            conn = createConnection();
            stmt = conn.createStatement();
            result = stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(stmt,conn);
        }
        return result;
    }
}
