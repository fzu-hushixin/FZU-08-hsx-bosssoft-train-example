package com.bosssoft.hr.train.thread;


import java.util.*;
import java.util.concurrent.*;



public class ThreadPool {
    //线程池最大线程数
    private int maximumPoolSize;
    //线程池的核心线程数
    private int corePoolSize;
    //存放阻塞任务的队列
    private BlockingQueue<Runnable> workQueue = null;
    //工作列表
    private HashSet<Worker> workers = new HashSet<>();
    //线程列表
    private List<Thread> threadList = new ArrayList<>();

    public ThreadPool(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
        workQueue = new LinkedBlockingQueue<>(maximumPoolSize);
    }

    public void newThread(Runnable runnable) {
        corePoolSize++;
        workQueue.offer(runnable);
        Worker worker = new Worker(workQueue, true);
        workers.add(worker);
        Thread thread = new Thread(worker);
        threadList.add(thread);
        try {
            thread.start();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void submit(Runnable runnable) {
        if(runnable == null) {
            throw new NullPointerException();
        }
        if(corePoolSize < maximumPoolSize) {
            newThread(runnable);
        }
        else {
            try {
                workQueue.put(runnable);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown() {
        for(Thread workerThread:threadList) {
            workerThread.interrupt();
        }
    }

}
