package com.bosssoft.hr.train.socket;



import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @param
 * @author: Administrator
 * @create: 2020-05-28 22:25
 * @since
 **/

public class ClientSocket implements Starter {
    private volatile boolean stop = false;

    @Override
    public boolean start() throws IOException {
        Selector selector = Selector.open();
        SocketChannel channel = SocketChannel.open();
        // 设置为非阻塞模式
        channel.configureBlocking(false);
        if (channel.connect(new InetSocketAddress("127.0.0.1", 8080))) {
            channel.register(selector, SelectionKey.OP_READ);
            //发送消息
            doWrite(channel, "hello server");
        } else {
            channel.register(selector, SelectionKey.OP_CONNECT);
        }

        while (!stop) {
            selector.select(1000);
            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> it = keys.iterator();
            SelectionKey key = null;
            while (it.hasNext()) {
                key = it.next();
                it.remove();
                SocketChannel sc = (SocketChannel) key.channel();
                if (key.isConnectable()) {
                    if (channel.finishConnect()) {
                        sc.register(selector, SelectionKey.OP_READ);
                        doWrite(channel, "hello server");
                    } else {
                        System.exit(1);
                    }
                }
                if (key.isReadable()) {
                    //读取服务端的响应
                    ByteBuffer buffer = ByteBuffer.allocate(1024);
                    int readBytes = sc.read(buffer);
                    String content = "";
                    if (readBytes > 0) {
                        buffer.flip();
                        byte[] bytes = new byte[buffer.remaining()];
                        buffer.get(bytes);
                        content += new String(bytes);
                        stop = true;
                    } else if (readBytes < 0) {
                        key.channel();
                        sc.close();
                    }
                    System.out.println(content);
                    key.interestOps(SelectionKey.OP_READ);
                }
            }
        }
        return true;
    }

    private  void doWrite(SocketChannel socketChannel,String data) throws IOException{
        byte[] req =data.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.allocate(req.length);
        byteBuffer.put(req);
        byteBuffer.flip();
        socketChannel.write(byteBuffer);
    }
}
