package com.bosssoft.hr.train.xml;

import com.bosssoft.hr.train.pojo.Student;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.List;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:13
 * @since
 **/
public class DOMOperation implements XMLOperation<Student> {
    private  static final String path = "src/test/resources/student.xml";
    private static final String nodes="/students/student";

    private static Document load(String filename) {
        Document document = null;
        try {
            SAXReader saxReader = new SAXReader();
            document = saxReader.read(new File(filename)); // 读取XML文件,获得document对象
        } catch (Exception e) {
            e.printStackTrace();
        }
        return document;
    }

    private static boolean store(Document document){
        try{
            XMLWriter writer = new XMLWriter(
                    new OutputStreamWriter(new FileOutputStream(DOMOperation.path)));
            writer.write(document);
            writer.close();
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    @Override
    public boolean create(Student object) {
        Document document = load(path);
        Element root = document.getRootElement();
        Element element = root.addElement("student");
        element.addAttribute("id", object.getId().toString());
        Element age = element.addElement("age");
        age.setText(String.valueOf(object.getAge()));
        Element name = element.addElement("name");
        name.setText(object.getName());
        return store(document);
    }

    @Override
    public boolean remove(Student object) {
        Document document = load(path);
        Element root = document.getRootElement();
        List studentList = document.selectNodes(nodes);
        Iterator iterator = studentList.iterator();
        while(iterator.hasNext()){
            Element studentElement = (Element)iterator.next();
            Element age = studentElement.element("age");
            Element name = studentElement.element("name");
            String ageContent = age.getText();
            String nameContent = name.getText();
            if(ageContent.equals("")  &&
                    Integer.valueOf(ageContent) == object.getAge() && nameContent.equals(object.getName())){
                root.remove(studentElement);
                store(document);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean update(Student object) {
        Document document = load(path);
        List studentList = document.selectNodes(nodes);
        Iterator iterator = studentList.iterator();
        while(iterator.hasNext()){
            Element studentElement = (Element)iterator.next();
            Element age = studentElement.element("age");
            Element name = studentElement.element("name");
            String nameContent = name.getText();
            //姓名判断
            if(nameContent.equals(object.getName())){
                age.setText(String.valueOf(object.getAge()));
                name.setText(object.getName());
                store(document);
                return true;
            }
        }
        return false;
    }

    //根据id来获取
    @Override
    public Student query(Student object) {
        Document document = load(path);
        List studentList = document.selectNodes(nodes);
        Iterator iterator = studentList.iterator();
        while(iterator.hasNext()){
            Element studentElement = (Element)iterator.next();
            if(Integer.valueOf(studentElement.attribute("id").getValue())==object.getId()){
                Element age = studentElement.element("age");
                Element name = studentElement.element("name");
                object.setAge(Integer.valueOf(age.getText()));
                object.setName(name.getText());
            }

        }
        return object;
    }
}
