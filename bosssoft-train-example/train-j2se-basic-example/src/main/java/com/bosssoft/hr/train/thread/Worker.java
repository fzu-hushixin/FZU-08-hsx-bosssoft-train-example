package com.bosssoft.hr.train.thread;

import java.util.concurrent.BlockingQueue;

public class Worker implements Runnable {
    BlockingQueue<Runnable> queue;
    boolean running;

    public Worker(BlockingQueue<Runnable> queue, boolean running) {
        this.queue = queue;
        this.running = running;
    }

    public Runnable getTask() throws InterruptedException {
        return queue.take();
    }

    @Override
    public void run() {
        while (!Thread.interrupted() && running){
            Runnable task = null;
            try {
                task = getTask();
                task.run();
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            }
        }
    }
}
