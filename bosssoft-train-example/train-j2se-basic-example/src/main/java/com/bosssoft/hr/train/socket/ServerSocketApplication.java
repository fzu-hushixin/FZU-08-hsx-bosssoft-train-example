package com.bosssoft.hr.train.socket;

import java.io.IOException;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:16
 * @since
 **/
public class ServerSocketApplication {
    public static  void main(String[] args) throws Exception {
        Starter serverSocket=new NIOServerSocket();
        serverSocket.start();
    }
}
